// Import required modules
const csv = require("csv-parser");
const path = require("path");
const fs = require("fs");

// Define the path to the deliveries.csv file
const filePath = path.join(__dirname, "..", "data", "deliveries.csv");

// Array to store delivery data
const results = [];

// Read the CSV file
fs.createReadStream(filePath)
  .pipe(csv())
  .on("data", (data) => results.push(data)) // Process each row of data
  .on("end", () => {

    // Calculate runs and balls for each bowler in super overs
    let bowlersEco = results.reduce((acc, delivery) => {
      if (Number(delivery["is_super_over"])) {
        if (acc[delivery["bowler"]] === undefined) {
          acc[delivery["bowler"]] = {
            runs: Number(delivery["total_runs"]),
            balls: 1,
          };
        } else {
          acc[delivery["bowler"]]["runs"] += Number(delivery["total_runs"]);
          acc[delivery["bowler"]]["balls"] += 1;
        }
      }
      return acc;
    }, {});

    // Calculate economy for each bowler
    Object.keys(bowlersEco).forEach((bowler) => {
      bowlersEco[bowler]["economy"] = bowlerEconomy(
        bowlersEco[bowler]["runs"],
        bowlersEco[bowler]["balls"]
      );
    });

    // Sort bowlers based on economy in ascending order
    let sortedEco = Object.keys(bowlersEco).sort((bowlerA, bowlerB) => {
      return (
        Number(bowlersEco[bowlerA]["economy"]) -
        Number(bowlersEco[bowlerB]["economy"])
      );
    });

    // Identify the bowler with the best economy
    let bestBowler = {
      name: sortedEco[0],
      economy: bowlersEco[sortedEco[0]]["economy"],
    };

    // Define the path to the output JSON file
    const resultPath = path.join(
      __dirname,
      "..",
      "public",
      "output",
      "bestBowlerInSuperOver.json"
    );

    // Write the calculated data to the output JSON file
    fs.writeFileSync(resultPath, JSON.stringify(bestBowler, null, 2));
  });

// Function to calculate the bowler's economy
function bowlerEconomy(runs, balls) {
  if (balls === 0) {
    return 0;
  } else {
    return ((runs / balls) * 6).toFixed(2);
  }
}
