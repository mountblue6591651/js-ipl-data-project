// Import required modules
const csv = require("csv-parser");
const path = require("path");
const fs = require("fs");

// Define the path to the deliveries.csv file
const filePath = path.join(__dirname, "..", "data", "deliveries.csv");

// Array to store match data
const results = [];

// Read the CSV file
fs.createReadStream(filePath)
  .pipe(csv())
  .on("data", (data) => results.push(data)) // Process each row of data
  .on("end", () => {
    // Create an object to store dismissal data
    let playersDismissal = results.reduce((acc, delivery) => {
      // Extract relevant data from each delivery
      let dismissedPlayer = delivery["player_dismissed"];
      let bowler = delivery["bowler"];
      let fielder = delivery["fielder"];

      // Check if a player was dismissed
      if (dismissedPlayer) {
        let dismissedPlayers = Object.keys(acc);
        // Check if the dismissed player already exists in the object
        if (dismissedPlayers.includes(dismissedPlayer)) {
          // Update dismissal count based on fielder or bowler
          if (fielder) {
            if (acc[dismissedPlayer][fielder]) {
              acc[dismissedPlayer][fielder] += 1;
            } else {
              acc[dismissedPlayer][fielder] = 1;
            }
          } else {
            if (acc[dismissedPlayer][bowler]) {
              acc[dismissedPlayer][bowler] += 1;
            } else {
              acc[dismissedPlayer][bowler] = 1;
            }
          }
        } else {
          // Add new entry for the dismissed player
          if (fielder) {
            acc[dismissedPlayer] = { [fielder]: 1 };
          } else {
            acc[dismissedPlayer] = { [bowler]: 1 };
          }
        }
      }

      return acc;
    }, {});

    // Get the list of dismissed players
    let dismissals = Object.keys(playersDismissal);

    // Object to store information about maximum dismissals
    let maxDismissals = {
      PlayerDismissed: null,
      PlayerWhoDismissed: null,
      count: 0,
    };

    // Iterate through each dismissed player and their opponents
    dismissals.forEach((player) => {
      let opponents = Object.keys(playersDismissal[player]);

      opponents.forEach((opponent) => {
        let dismissalsCount = playersDismissal[player][opponent];

        // Update maxDismissals if the current count is higher
        if (dismissalsCount > maxDismissals.count) {
          maxDismissals.PlayerDismissed = player;
          maxDismissals.PlayerWhoDismissed = opponent;
          maxDismissals.count = dismissalsCount;
        }
      });
    });

    // Define the path to the output JSON file
    const resultPath = path.join(
      __dirname,
      "..",
      "public",
      "output",
      "onePlayerDismissedAnother.json"
    );

    // Write the calculated data to the output JSON file
    fs.writeFileSync(resultPath, JSON.stringify(maxDismissals, null, 2));
  });
