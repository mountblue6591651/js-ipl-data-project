// Import required modules
const csv = require("csv-parser");
const path = require("path");
const fs = require("fs");

// Define the path to the matches.csv file
const filePath = path.join(__dirname, "..", "data", "matches.csv");

// Array to store match data
const results = [];

// Read the CSV file
fs.createReadStream(filePath)
  .pipe(csv())
  .on("data", (data) => results.push(data)) // Process each row of data
  .on("end", () => {
    // Calculate matches per year
    let result = results.reduce((acc, match) => {
      let seasons = Object.keys(acc);
      if (seasons.includes(match["season"])) { //Check if accumulator already has the year, if not add the year
        acc[match["season"]] = acc[match["season"]] + 1;
      } else {
        acc[match["season"]] = 1;
      }
      return acc;
    }, {});

    // Define the path to the output JSON file
    const resultPath = path.join(
      __dirname,
      "..",
      "public",
      "output",
      "matchesPerYear.json"
    );

    // Write the calculated data to the output JSON file
    fs.writeFileSync(resultPath, JSON.stringify(result, null, 2));
  });
