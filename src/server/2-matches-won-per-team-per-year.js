// Import required modules
const csv = require("csv-parser");
const path = require("path");
const fs = require("fs");

// Define the path to the CSV file
const filePath = path.join(__dirname, "..", "data", "matches.csv");

// Array to store match data
const results = [];

// Read the matches.csv file
fs.createReadStream(filePath)
  .pipe(csv())
  .on("data", (data) => results.push(data)) // Process each row of data
  .on("end", () => {
    // Calculate matches won per team per year
    let result = results.reduce((acc, match) => {
      let team = Object.keys(acc);
      if (team.includes(match["winner"])) { // Check if accumulator contains the team name else add that team name
        let years = Object.keys(acc[match["winner"]]);
        if (years.includes(match["season"])) { // Check if specific year is present in specific team or not else add the year
          acc[match["winner"]][match["season"]] =
            acc[match["winner"]][match["season"]] + 1;
        } else {
          acc[match["winner"]][match["season"]] = 1;
        }
      } else {
        acc[match["winner"]] = { [match["season"]]: 1 };
      }
      return acc;
    }, {});

    // Define the path to the output JSON file
    const resultPath = path.join(
      __dirname,
      "..",
      "public",
      "output",
      "matchesWonPerTeamPerYear.json"
    );

    // Write the calculated data to the output JSON file
    fs.writeFileSync(resultPath, JSON.stringify(result, null, 2));
  });
