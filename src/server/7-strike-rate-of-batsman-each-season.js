// Import required modules
const csv = require("csv-parser");
const path = require("path");
const fs = require("fs");

// Define the path to the matches.csv file
const matchesPath = path.join(__dirname, "..", "data", "matches.csv");

// Define the path to the matches.csv file
const deliveriesPath = path.join(__dirname, "..", "data", "deliveries.csv");

// Array to store match data and deliveries data
const matches = [];
const deliveries = [];

// Read the CSV file for matches
fs.createReadStream(matchesPath)
  .pipe(csv())
  .on("data", (data) => matches.push(data)) // Process each row of data
  .on("end", () => {
    // Read the CSV file for deliveries
    fs.createReadStream(deliveriesPath)
      .pipe(csv())
      .on("data", (data) => deliveries.push(data)) // Process each row of data
      .on("end", () => {
        // Create a map to associate match IDs with seasons
        let seasonIdMap = matches.reduce((acc, match) => {
          acc[match["id"]] = match["season"];
          return acc;
        }, {});

        // Calculate runs and balls per batsman for each season
        let runPerBatsman = deliveries.reduce((acc, delivery) => {
          let season = seasonIdMap[delivery["match_id"]];
          let batsman = delivery["batsman"];

          if (acc[batsman]) {
            if (acc[batsman][season]) {
              // If the batsman and season entry already exists, update Runs and Balls
              acc[batsman][season]["Runs"] += Number(delivery["total_runs"]);
              acc[batsman][season]["Balls"] = acc[batsman][season]["Balls"] + 1;
            } else {
              // If the batsman exists but not for the current season, create a new entry
              acc[batsman][season] = {
                Runs: Number(delivery["total_runs"]),
                Balls: 1,
              };
            }
          } else {
            // If the batsman doesn't exist, create a new entry for the batsman and season
            acc[batsman] = {
              [season]: { Runs: Number(delivery["total_runs"]), Balls: 1 },
            };
          }
          return acc;
        }, {});

        // Calculate the strike rate for each batsman for each season
        let batsmenStrikeRates = Object.keys(runPerBatsman).reduce(
          (acc, batsman) => {
            let years = Object.keys(runPerBatsman[batsman]);
            let strikeRatePerYear = years.reduce((acc, year) => {
              // Calculate the strike rate and store it in the accumulator
              let strikeRate =
                (runPerBatsman[batsman][year]["Runs"] /
                  runPerBatsman[batsman][year]["Balls"]) *
                100;
              acc[year] = { "Strike Rate": strikeRate };
              return acc;
            }, {});
            acc[batsman] = strikeRatePerYear;
            return acc;
          },
          {}
        );

        // Write the calculated data to the output JSON file
        const resultPath = path.join(
          __dirname,
          "..",
          "public",
          "output",
          "strikeRateOfBatsmanEachSeason.json"
        );
        fs.writeFileSync(
          resultPath,
          JSON.stringify(batsmenStrikeRates, null, 2)
        );
      });
  });
