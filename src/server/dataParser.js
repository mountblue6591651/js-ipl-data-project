const path = require('path');
const fs = require('fs');

const filePath = path.join(__dirname, '..', 'data', 'matches.csv');

var lines = fs.readFileSync(filePath)
    .toString()
    .split('\n')
    .map(e => e.trim());

// Extract headers from the first line
const headers = lines[0].split(',').map(e => e.trim());

// Create an array of objects
var data = lines.slice(1).map(line => {
    const values = line.split(',').map(e => e.trim());
    return headers.reduce((obj, header, index) => {
        obj[header] = values[index];
        return obj;
    }, {});
});


module.exports = data;