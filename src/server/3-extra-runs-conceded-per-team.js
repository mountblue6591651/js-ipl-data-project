// Import required modules
const csv = require("csv-parser");
const path = require("path");
const fs = require("fs");

// Define paths to CSV files
const deliveriesPath = path.join(__dirname, "..", "data", "deliveries.csv");
const matchesPath = path.join(__dirname, "..", "data", "matches.csv");

// Array to store match data
const results = [];

// Read matches.csv file
fs.createReadStream(matchesPath)
  .pipe(csv())
  .on("data", (data) => results.push(data)) // Process each row of data
  .on("end", () => {
    // Extract match IDs for the year 2016
    let id = results
      .filter((match) => {
        if (match["season"] === "2016") {
          return true;
        }
      })
      .map((match) => {
        return match["id"];
      });

    // Array to store delivery data
    let matchData = [];

    // Read deliveries.csv file
    fs.createReadStream(deliveriesPath)
      .pipe(csv())
      .on("data", (data) => matchData.push(data)) // Process each row of data
      .on("end", () => {
        // Calculate extra runs conceded per team
        let extraRuns = matchData.reduce((acc, delivery) => {
          if (id.includes(delivery["match_id"])) {
            let teams = Object.keys(acc);
            if (teams.includes(delivery["batting_team"])) {
              acc[delivery["batting_team"]] =
                acc[delivery["batting_team"]] + Number(delivery["extra_runs"]);
            } else {
              acc[delivery["batting_team"]] = Number(delivery["extra_runs"]);
            }
          }
          return acc;
        }, {});

        // Define path to output JSON file
        const resultPath = path.join(
          __dirname,
          "..",
          "public",
          "output",
          "extraRunsConcededPerTeam.json"
        );

        // Write the calculated data to the output JSON file
        fs.writeFileSync(resultPath, JSON.stringify(extraRuns, null, 2));
      });
  });
