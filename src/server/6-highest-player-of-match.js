// Import required modules
const csv = require("csv-parser");
const path = require("path");
const fs = require("fs");

// Define the path to the matches.csv file
const filePath = path.join(__dirname, "..", "data", "matches.csv");

// Array to store match data
const results = [];

// Read the CSV file
fs.createReadStream(filePath)
  .pipe(csv())
  .on("data", (data) => results.push(data)) // Process each row of data
  .on("end", () => {

    let result = results.reduce((acc, match) => { // use reduce() to map out all the player of matches of every season in an object
      let season = Object.keys(acc);
      if(season.includes(match['season'])){
        let players = Object.keys(acc[match['season']]);
        if(players.includes(match['player_of_match'])){
          acc[match['season']][match['player_of_match']] = acc[match['season']][match['player_of_match']] + 1; // if the player is present in the season increase the number of times he won player of match
        }
        else{
          acc[match['season']][match['player_of_match']] = 1; // create the object containing the player of match name in a specific season
        }
      }
      else{
        acc[match['season']] = {[match['player_of_match']] : 1}; // create an object in accumulator with key as the season if the season is not present in the accumulator
      }
      return acc;
    }, {});

    let years = Object.keys(result);

    let topPlayers = years.reduce((acc, season) =>{ // Access the players season-wise
      let players = Object.keys(result[season]);
      let bestPlayerOfSeason = players[0];

      players.forEach(player => {
        if(result[season][player] > result[season][bestPlayerOfSeason]){ // Iterate through each player and find out player with most player of match
          bestPlayerOfSeason = player;
        }
      });
      
      acc[season] = bestPlayerOfSeason;
      return acc;

    },{});


    // Define the path to the output JSON file
    const resultPath = path.join(
      __dirname,
      "..",
      "public",
      "output",
      "highestPlayerOfMatch.json"
    );

    // Write the calculated data to the output JSON file
    fs.writeFileSync(resultPath, JSON.stringify(topPlayers, null, 2));
  });
