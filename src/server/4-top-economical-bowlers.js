// Import required modules
const csv = require("csv-parser");
const path = require("path");
const fs = require("fs");

// Define paths to CSV files
const deliveriesPath = path.join(__dirname, "..", "data", "deliveries.csv");
const matchesPath = path.join(__dirname, "..", "data", "matches.csv");

// Array to store match data
const results = [];

// Read matches.csv file
fs.createReadStream(matchesPath)
  .pipe(csv())
  .on("data", (data) => results.push(data)) // Process each row of data
  .on("end", () => {
    // Extract match IDs for the year 2015
    let id = results
      .filter((match) => {
        if (match["season"] === "2015") {
          return true;
        }
      })
      .map((match) => {
        return match["id"];
      });

    // Array to store delivery data
    let matchData = [];

    // Read deliveries.csv file
    fs.createReadStream(deliveriesPath)
      .pipe(csv())
      .on("data", (data) => matchData.push(data)) // Process each row of data
      .on("end", () => {
        
        // Calculate runs and balls bowled by each bowler
        let bowlerData = matchData.reduce((acc, delivery) => {
          if (id.includes(delivery['match_id'])) {
            let bowlers = Object.keys(acc);
            if (bowlers.includes(delivery['bowler'])) {
              acc[delivery['bowler']]['runs'] = acc[delivery['bowler']]['runs'] + Number(delivery['total_runs']);
              acc[delivery['bowler']]['balls'] = acc[delivery['bowler']]['balls'] + 1;
            } else {
              acc[delivery['bowler']] = { runs: Number([delivery['total_runs']]), balls: 1 };
            }
          }
          return acc;
        }, {});

        // Calculate economy and identify the top 10 economical bowlers
        let bowlers = Object.keys(bowlerData);

        let topBowlers = bowlers.reduce((acc, bowler) => { //used reduce() to calculate and store the economy of every bowler
          let eco = (bowlerData[bowler]['runs'] / bowlerData[bowler]['balls']) * 6.0;
          acc.push({ [bowler]: eco });
          return acc;
        }, []).sort((bowler1, bowler2) => { // used sort() to sort the bowlers according to their economy
          let eco1 = Object.values(bowler1)[0];
          let eco2 = Object.values(bowler2)[0];

          if (eco1 > eco2) {
            return 1;
          } else {
            return -1;
          }

        }).filter((bowler, index) => { // used filter() to filter out top 10 bowlers only
          if (index < 10) {
            return true;
          }
        }).map((bowler) => { // used map() to just take out the names of the bowlers from the object.
          return Object.keys(bowler)[0];
        });

        // Define path to output JSON file
        const resultPath = path.join(
          __dirname,
          "..",
          "public",
          "output",
          "topEconomicalBowlers.json"
        );

        // Write the calculated data to the output JSON file
        fs.writeFileSync(resultPath, JSON.stringify(topBowlers, null, 2));
      });
  });
