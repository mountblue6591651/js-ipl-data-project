// Import required modules
const csv = require("csv-parser");
const path = require("path");
const fs = require("fs");

// Define the path to the matches.csv file
const filePath = path.join(__dirname, "..", "data", "matches.csv");

// Array to store match data
const results = [];

// Read the CSV file
fs.createReadStream(filePath)
  .pipe(csv())
  .on("data", (data) => results.push(data)) // Process each row of data
  .on("end", () => {

    let result = results.reduce((acc, match) => {
        let teams = Object.keys(acc);
        if(teams.includes(match['toss_winner'])){  
          if(match['toss_winner'] === match['winner']){
            acc[match['toss_winner']] = acc[match['toss_winner']] + 1;
          }
        }
        else{
          if(match['toss_winner'] === match['winner']){
            acc[match['toss_winner']] = 1;
          }
          else{
            acc[match['toss_winner']] = 0;
          }
        }
        return acc;
    }, {});

    // Define the path to the output JSON file
    const resultPath = path.join(
      __dirname,
      "..",
      "public",
      "output",
      "wonTossWonMatch.json"
    );

    // Write the calculated data to the output JSON file
    fs.writeFileSync(resultPath, JSON.stringify(result, null, 2));
  });
